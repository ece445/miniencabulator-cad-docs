﻿# CAD Assignment

**Last updated 2021 for KiCAD 5.1.10.**

In this assignment, you will be designing a PCB for a mini encabulator.  Compared to a [full-sized retro encabulator](https://www.youtube.com/watch?v=RXJKdh1KZ0w), this simplified device only does two things:

- blinks an LED when the user holds down a button
- sets blink rate proportional to potentiometer position

![Miniencabulator visual aid](encabulator.svg ':size=300')

<!-- We will use an ATTiny85 in this assignment, which is a smaller version of the ATMega328 found in the Arduino Uno/Nano and used in many ECE445 projects.  Later on in this class, you will learn to program this microcontroller and assemble your own encabulator. -->

You will be using KiCAD (pronounced KEY-cad), a design tool for creating electrical schematics and and board layouts.  This assignment will have 3 stages:

1. Add components and define the electrical connections of your design in a **schematic**
2. Assign packages/footprints to the components in your schematic.  (e.g. ATTiny85 can be purchased in a [SO-8/SOIC-8][SOIC8] or [DIP-8][DIP8] package)
3. Define PCB dimensions, component locations, and copper traces in a **layout**

[DIP8]: https://upload.wikimedia.org/wikipedia/commons/c/c7/150-8-DIP.jpg
[SOIC8]: https://upload.wikimedia.org/wikipedia/commons/2/2a/MFrey_SOIC8.JPG

# Schematic

Begin by installing [KiCAD 5.1.10](https://www.kicad.org/download/) or newer.  Create a new project called `[netid]_miniencabulator` and open `[netid]_miniencabulator.sch`.


## Microcontroller Setup

Hit `a` to open the component add dialog.  Search "attiny85 so" for an `ATTiny85-20S` in an SO package and left click to place the part.

![Adding components to schematic](sch_search.svg)

Add these other components and arrange like in the picture below:

- `AVR-ISP-6` - programming header
- `MBR0520` - programmer reverse current protection
- `+5V` - generic 5V power symbol
- `GND` - generic ground power symbol
- `R` - generic resistor
- `C_Small` - generic capacitor

Move components around by hovering over them with the mouse and pressing `g` (grab).  Components can be copied with `c`.

![](sch1.png)

Begin connecting components as in the picture below. Use `w` to start routing a new wire under the mouse, left-click to finish routing, and `Esc` to cancel.

To reduce clutter, will use labels to connect the ISP header to the microcontroller.  Use `l` to place labels on pins or wires.  Any two points with the same label will behave as if connected by a wire.

Set the value of the resistor to 10k and the capacitor to 1uF by hovering over each and pressing `v`.

![](sch2.png)

This is the minimum circuitry needed to program an ATTiny85 with an external programmer.

## Power

Add these components to a blank area of the schematic and arrange as in the picture below:

- `Conn_01x02` (search "conn gen") - power supply connector
- `LM1117-5.0` - voltage regulator
- `+5V` - generic power symbol
- `GND` - generic power symbol

![](sch_power.png)

Set the value of the connector to "Vin" using `v`.  When we layout the PCB later, we will tell KiCAD to display this text on the board silkscreen next to the connector.

The convention to follow when placing components in a block is for power/signal to flow from left to right.

## Peripherals

It's now time to add the peripheral electronics to our encabulator.  We will connect the button, LED, and potentiometer to the microcontroller.

It's worth mentioning button bounce, which is a problem inherent in all mechanical buttons where the button makes intermittent contact before finally closing.  This can be a problem for software which looks for a voltage transition on an IO pin to trigger some action, as there may be multiple transitions when the user presses the button only once.  Filtering out these spurious transitions is known as **debouncing**, and can be implemented in hardware or software.

We will add a connector for our button with a simple hardware debouncer.  Add these components:

- `Conn_01x02` (search "conn gen") - button connector
- `R` - RC filter resistor
- `C` - RC filter capacitor

![Low-pass filter debouncer](sch_debounce.png)
![Connect to ATTiny85](sch_debounce2.png)

Change the value of the connector to "BUTTON" and place "BUTTON" labels on the output of the filter and input to microcontroller.  Values of 10k and 1uF will give a time constant of about 10ms, which should be long enough to filter out bounces.

![Filtering out button bounce with a hardware "debounce" circuit](debounce.svg)

Next, add these components to connect the potentiometer and LED.

- `Conn_01x03` (search "conn gen") - potentiometer connector
- `Conn_01x02` (search "conn gen") - LED connector
- `R` - current limiting resistor

A value of 1k should suffice for the LED current limiting.  

It's important that the potentiometer be connected to an ADC pin of the ATTiny85.  Refer to the [AVR page](microcontrollers/avr.md) on the wiki or the datasheet.

![](sch_peripherals.png)

<!-- Note that a footprint is already defined for this part.  Not all components are like this.  For example, we will need to choose footprints for resistors, capacitors, and connectors before starting the layout later on. -->

## Finalizing Schematic

Before moving on to the next stage, we will clean up and document our schematic.  Since screenshots of your schematics will go into your design document, you should put some effort into keeping them neat and easy to read.  Use the graphic line tool to divide up your schematic.  Use the text tool to label each of these blocks.

Additionally, add four mounting holes:

- `MountingHole` - hole for mounting in enclosure

![Graphic line and text tools](sch_graphictools.svg)
![Complete schematic](schematic.png)

# Component Association

It's time to tell KiCAD what footprints to use for parts.  Click the "Run footprint assignment tool" button on the top toolbar:

![Associate PCB footprints to schematic symbols](associate_button.svg)

KiCAD sees that our schematic contains unannotated symbols ("R?", "C?", "U?", etc), and asks how we want to number them.  The default options are fine here.  Any time you add new symbols to your schematic, KiCAD will bring up this dialog again.

After clicking "Annotate", KiCAD will bring up the footprint association window, containing three panes.  The left pane contains the categories of footprints, the middle pane contains your parts that need footprints assigned to them, and the right pane contains available footprints based on your filter options.

We'll start with the capacitors.  Hold `Ctrl` and select both capacitors in the middle pane.  We have 0805 sized capacitors that we will solder by hand, so double-click `Capacitor_SMD:C_0805_..._HandSolder`.  Pay attention to the filter options at the top of the window.

![Footprint association window.  The first two filters are generally appropriate](associate.svg)

Associate the remaining parts with these footprints:

- resistors (R*) - `Resistor_SMD:R_0805_...HandSolder`
- mounting holes (H*) - `MountingHole:MountingHole_3.2mm_M3` (for [M3 screws][M3])
- 2 pin connectors (Vin, BUTTON, LED) - `Connector_Molex:KK-254...1x02...`
- 3 pin connectors (POT) - `Connector_Molex:KK-254...1x03...`
- programming header (AVR-ISP-6) - `Connector_IDC:IDC-Header_2x03...Vertical`
- regulator (LM1117-5.0) - `Package_TO_SOT_SMD:SOT-223`

Note that `Package_TO_SOT_SMD:SOT-223` has 4 pins, but the LM1117 symbol only has 3.  You must uncheck the pin count filter for it to show up.


[M3]: https://upload.wikimedia.org/wikipedia/commons/5/5f/Screw_from_computers.jpg

Footprints are now associated with our symbols.  Click the "Open PCB in board editor" button to start working on the layout.

![Switch to layout from schematic](sch_brdbutton.png)

# Layout

## Component Placement

It's time to start placing components on our board.  Start by clicking the "Update PCB with changes made to schematic" button and place the footprints somewhere in the middle of the page.  If you make changes to your schematic later on, you should use this button.

![Pull updates from schematic](brd_update.png)
![](brd_update2.png)

The white lines are called **unrouted nets** (also known as "airwires") and indicate what component pads should be electrically connected.

![](brd.png)

To make things a bit easier, we will add a ground plane to the front and back side of our board that will automatically connect all GND nets together for us.  Any areas of filled in copper like this are known as **zones** in KiCAD.

![Zone add tool](brd_zone.png)
![Zone add dialog](brd_zone2.png)

You can also turn off the `F.Fab` and `B.Fab` layers in the right toolbar to hide some unnecessary text:

![](brd_ffab.png)

![F.Cu and B.Cu zones added around our working area. GND nets are hidden now.](brd_zone3.png)

Now we can begin sorting the footprints.  This is generally where most of the time designing a PCB should be spent.  The goal here is to untangle the footprints so that airwires cross as little as possible.  This makes routing in the next section simpler and helps to keep traces short.

When working on new boards, a good way to start is to first group the footprints together the same way they are grouped in the schematic.

![Keep your schematic and board open at the same time when arranging footprints](brd_dual.png)

To save some time, below is a suggested layout for your board:

![Suggested board layout](brd_suggested.png)

<!-- ![Zone visibility settings](brd_zone4.png) -->

When you've placed all your components, make sure the component references are visible and not obscured by other footprints.  You will use these references when populating the board.

![Component reference is obscured by other component](brd_obscured.png)

Next, we will add some labelling so we know what are connectors are.  Click on each connector and press `e` to bring up the footprint properties window.  Hide the footprint reference and change the layer of the **value** field to `F.Silkscreen` so it shows up on the front side silkscreen.  

For connectors which have polarity, you should label the individual pins.  For the `Vin` and `LED` connectors, add 2 new properties `+` and `-` and set their layer to the front silkscreen.

(If your newly-added labels are hidden behind the footprint, you can choose `F.Silkscreen` in the layers pane to get them to show up.)

![Footprint edit dialog.  Add silkscreen labelling for your connectors here](brd_connlabel.png)

Use the text tool to add information about your board to the silkscreen layer.  Include the date, your team number, and the board version.  

![Graphic text tool](brd_text.png)

![Board with labelling](brd_mostlycomplete.png)

Then drag your front and back ground zones to surround the component area.  It's a good idea to make these zones as small as possible to limit RF noise picked up from external sources.

![Zones surrounding component area](brd_mostlycomplete2.png)

It's time to pick an enclosure.  Generally you want to pick one which is slightly wider/longer than your PCB, has sufficient height for PCB connectors and panel mount components and comes with PCB mounting bosses.  Check the enclosure datasheet.

For example, the SN25 enclosure from Polycase is shown below:

![](sn25.jpg)
![](sn25_2.png)

So the mounting bosses are separated by 1.26in vertically and 1.575in horizontally.  Change the grid size to 5mils (0.005in) and move the mounting holes to the corners of the board.  Use the dimensioning tool to verify that the separation between the mounting holes is correct.  The dimensioning lines should go on the `User.Drawings` layer. 

When your mouse is above the mounting hole, snapping crosshairs should appear.  These dimensions must be exact to receive full credit.

![Grid size.  (remember to change this back!)](brd_holes.png)
![Dimensioning tool](brd_holes2.png)

![](brd_mostlycomplete3.png)

Finally, use the rectangle tool with the `Edge.Cuts` layer selected and draw a rectangle just outside of the mounting holes to define the edge of the board.

![Draw board outline](brd_outline.png)

![A board ready for routing](brd_mostlycomplete4.png)

## Routing

Keyboard shortcuts used in this section:
- `x` - Add track
- `v` - Toggle front/back side of board
- `u` - Select all segments of a track
- `Del` - Delete selected track segments
- `b` - redraw zones (after moving footprints or tracks)

It's time to start connecting our components together with tracks (also known as traces).  KiCAD only comes with a single track width specified, so we will add more.

Click the dropdown `Track: ... > Edit pre-defined sizes...` and add track widths for 10, 20, and 30 mils.  These larger tracks are useful if you need to carry more current.

![Add more track widths](brd_tracks.png)
![](brd_tracks2.png)

When doing routing, it's good to prioritize traces which are high-speed, high-power, or highly parallel over other traces like slow data lines or simple GPIO in order to keep them short.

We'll start by routing the tracks from microcontroller to the ISP header.  Choose the 10 mil track width from the dropdown menu, and press `x` to begin routing.  Route the MOSI, MISO, SCK, and RST pads on the microcontroller to their respective pins on the programming header by left-clicking, as in the picture below.

![](brd_isp.png)

We'll use the back side of the board to connect the RST pullup resistor to 5V.  

Start routing the `+5V` pin on the microcontroller, then hover over the position indicated in the first picture below and press `v` to switch to the back side of the board and left-click to place a via.  Repeat this process in the position indicated in the second picture to come to the front side of the board and finish connecting the track.

![](brd_via.png)
![](brd_via2.png)

The red/green segments of this track are on the front/back sides of the board and are connected by a **via**.

Continue routing the rest of the tracks.  Use 20 mil tracks when connecting Vin to the regulator, and the regulator to the microcontroller, as in the picture below.

![](brd_routingcomplete.png)

## Copper Island and GND Stitching

You may notice there is still a remaining airwire in the previous picture.  This may or may not be present in your board depending on how you layed down your tracks. (continue reading even if you don't have this!)

![](brd_island.png)

This is because there is not enough space around the regulator to allow the GND zone to flow in around it.  You can use the zone visibility settings to see this more clearly. (hit `b` after changing visibility settings to redraw the zones)

![Zone visibility settings](brd_zone4.png)
![Not enough space for ground zone](brd_island2.png)

The unconnected bit of copper on the left is known as a **copper island**.  Instead of moving components around to get more space, we can manually add a via

![Add via tool](brd_island3.png)
![Via connects unconnected island to back side](brd_island4.png)

While you're at it, scatter a few GND vias around other areas of your board.  This is known as **ground stitching** and helps keep the current return path short between components.  The completed PCB should look something like below:

![Completed PCB](brd_routingcomplete2.png)

Submit your assignment by uploading the `.kicad_sch` and `.kicad_pcb` files.

# Appendix

## Generating Gerbers

While the PCB is finished, you can't just send your KiCAD files directly to a PCB board house (PCB production company).  Instead, we will tell KiCAD to compile our design into a set of files containing low-level instructions that can be understood by the fabhouse.  These files are known as **Gerbers**, and have file extension .grb and .drl.

Choose `File > Plot` to bring open the menu for generating Gerbers.  Create a new folder in your project's folder called `gerbers/`, and choose this as the output directly.

The default options should be OK for most board houses.  Choose `Generate drill files > Generate drill file` to create the .drl file, and `Plot` to create the .grb files.

Finally, zip the `gerbers/` folder.  This file is what you will upload when ordering a PCB.


## Custom Symbols

Occasionally, you may find the part you are looking for is not available in the KiCAD libraries.  Since creating custom symbols/footprints can be time consuming, thoroughly search the library for possible alternatives first.

We will create a symbol for the fictional ATMini99, a 6 pin microcontroller.  Open the symbol editor from the main project window.

Create a new symbol project library with `File > New Library`.  If your custom parts are going to be shared across several projects, see [the section below](sharing-custom-symbolsfootprints-between-boards).

Right click on your new library and choose `New Symbol`.

The ATTiny99 has the following pinout:

| Name  | Number |
|-------|--------|
| VCC   | 1      |
| GPIO1 | 2      |
| GPIO2 | 3      |
| GPIO3 | 4      |
| GPIO4 | 5      |
| GND   | 6      |

Begin by moving the symbol name and reference text out of the way, then use the `Add pins` tool to begin adding pins according to your datasheet.  The red circles are where wires connect, so these should face outward.

Try to group your pins together by their function.  For example, power pins go near the top and bottom of the part, while inputs and outputs go in the middle.

Finally, add graphic lines to complete the outline of your symbol.

![](custom_symbol.png)

## Custom Footprints

Open the footprint editor in the main project window and create a new footprint project library with `File > New Library`.

![](footprint_editor.png)

Right click on your new library and choose `New Footprint`.  Give the footprint a name.  Here we are making the fictional TO999 footprint.

Check near the bottom of your part's datasheet for the recommended footprint land pattern.  

![ATMini99 land pattern (in mm)](land_pattern.png)

Take note of whether the dimensions are given in mm or mils.  Set the footprint units to mm or in accordingly.  It is also sometimes convenient to set the grid equal to the spacing between pads.

![](custom_footprint.png)

Use the `Add pad` tool and place the first pad.  Hit `Esc`, hover over the pad and press `e` to bring up the pad properties.  Here you can set the pin number, pad type (SMD, through-hole), shape, and location.

![](custom_footprint2.png)

Right click on the first pad, and choose create array.  Use a calculator to compute the vertical/horizontal distances between pad centers, if necessary.

![Create Array dialog](custom_footprint3.png)

Finally, use the graphic line tool to draw the outline of the component body on the `F.Silk` layer.  You may want to adjust the grid size for this.

![](custom_footprint4.png)

Always do a final sanity check by checking the pin numbers and measuring the vertical/horizontal distance between the pads at the corners (and hole diameter if using through-hole).

## Sharing Custom Symbols/Footprints Between Boards

When sharing a common symbol or footprint library across several boards, put the library in the same folder as your projects.

e.g.

```
common.lib
common.pretty
main_board/
    main_board.kicad_pro
    main_board.kicad_sch
    main_board.kicad_brd
auxiliary_board/
    auxiliary_board.kicad_pro
    auxiliary_board.kicad_sch
    auxiliary_board.kicad_brd
```

Select `Preferences > Manage Symbol Libraries` and add the library as a "Project Specific Library".  Use `${KIPRJMOD}/../common.lib` as the library path.

![Add library window](common_library.png)

Repeat this for `Preferences > Manage Footprint Libraries` and use `${KIPRJMOD}/../common.pretty` as the path.

Adding libraries as a relative path in this way will ensure you can share your projects folder on Github/Dropbox/etc.

